#!/bin/sh

echo "creating example config..."
#cp /default.conf.example /etc/nginx/conf.d

echo "adding certbot cronjob..."
echo "00 00 * * * certbot -q renew >> /var/log/cron.log" > /etc/crontabs/root
