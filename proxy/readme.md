```shell
docker network create proxy
docker run -d --name proxy -p "443:443" -p "80:80" -v /$PWD/conf.d:/etc/nginx/conf.d --restart always 1creator/proxy
docker network connect proxy proxy
```
